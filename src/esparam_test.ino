// ESParam example / test
// for a esp32-s2 with an oled screen
// https://github.com/maxd/ESParam


#include "ESParam.h"
#include "ESParamPrinter.h"

#define STATUS_LED_PIN 2

// a simple parameter to control the led
BoolParam ledParam;

void setLED(bool _v){
    digitalWrite(STATUS_LED_PIN, !_v);
}

void setup(){
    Serial.begin(9600);
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, LOW);

    // setup the parameter
    ledParam.set("/device/led", 0);
    ledParam.saveType = SAVE_ON_REQUEST;
    ledParam.setCallback(setLED);
    paramCollector.add(&ledParam);

    // start the printer and esparam
    printer.begin();
    setupEsparam(&printer);
    startNetwork();

    digitalWrite(STATUS_LED_PIN, HIGH);
}


void loop(){
    updateEsparam();
}