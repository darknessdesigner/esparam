/*
esparam.h
Boiler plate to integrate Param library on the esp32
Features :
- basic set of parameters to configure network interfaces
- param websocket for web interface
- save and load parameters from esp32 flash
- receive OSC
- over the air flashing

@author maxd@nnvtn.ca
*/
#ifndef ESPARAM_H
#define ESPARAM_H

#include <ESParam_conf.h>

#include <WiFi.h>

#if ESPARAM_BOARD_HAS_ETHERNET
#include <ETH.h>
#endif


#if ESPARAM_ENABLE_OTA
    #include <ArduinoOTA.h> 
#endif

#include <ESPmDNS.h>

#if ESPARAM_ENABLE_WEBSERVER
    #include <ESPAsyncWebServer.h>
    #include <DNSServer.h>
#endif

#if ESPARAM_ENABLE_FS
    #include <FS.h>
    #include <LittleFS.h>
    #define FORMAT_FS_IF_FAILED 1
#endif

#include <Bounce2.h>
#include <Param.h>
#include <ParamJson.h>
#include <ParamUtils.h>

#if ESPARAM_ENABLE_OSC
    #include <MicroOsc.h>
    #include <MicroOscUdp.h>
    #include <ParamOsc.h>

    extern MicroOscUdp<ESPARAM_MICROSC_BUFFER_SIZE> myMicroOscUdp;
    extern bool oscInputFlag;
    extern WiFiUDP oscUdpSocket;
    extern IPAddress oscRemoteIP;
#endif // ESPARAM_ENABLE_OSC

typedef void (* MessageNotRecognizedCallbackFunction)(MicroOscMessage&);

void setMessageNotRecognizedCallbackFunction(MessageNotRecognizedCallbackFunction);

#define BUTTON_PIN 0
// conf struct
typedef struct {
    // wifi params
    BoolParam wifiEnableParam;
    BoolParam wifiDhcpParam;
    TextParam wifiStaticIpParam;
    TextParam wifiGatewayParam;
    TextParam wifiSubnetParam;
    TextParam wifiSsidParam;
    TextParam wifiPswdParam;
    TextParam deviceNameParam;
    BoolParam forceAccessPointParam;
#if ESPARAM_BOARD_HAS_ETHERNET
    // eth params
    BoolParam ethEnableParam;
    BoolParam ethDhcpParam;
    TextParam ethIpParam;
    TextParam ethGatewayParam;
    TextParam ethSubnetParam;
    IPAddress ethIp, ethGateway, ethSubnet;
#endif
    // other params
#if ESPARAM_ENABLE_OSC
    IntParam oscInPortParam;
    IntParam oscOutPortParam;
    TextParam oscOutIpParam;
#endif

    BangParam saveParam;
    BangParam rebootParam;
    BangParam deleteConfigParam;
} ESParamConf;

extern ESParamConf epConf;

// we provide a global ParamCollector to collect parameters throughout the code
extern ParamCollector paramCollector;
// namespace wt32_params;
void setupEsparam(Print * p);
void startNetwork();
void updateEsparam();
void sendToWeblog(char * _log);
void websocketSendMessage(char * _msg);
String getDeviceIp();
const char * getDeviceName();
const char * getDeviceSSID();
bool esparamIsAP();
#endif