/**
 * ##copyright##
 * See LICENSE.txt
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.05
 * @since     2022-03-23
 */
 
#include "ESParamPrinter.h"

namespace{
#if ESPARAM_BOARD_HAS_OLED
    #define SCREEN_WIDTH 128
    #define SCREEN_HEIGHT 64
    Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
#endif
    int logLineIdx;
    int logCharIdx;
    char logBuffer[LOG_LINE_ROW_COUNT][MAX_LOG_LINE_WIDTH];
    size_t webLogIdx;
    char webLog[WEB_LOG_MAX_LENGTH];
}

Printer printer;
Printer::Printer(){}

void Printer::begin(){
    // if(useOled){
    useOled = ESPARAM_BOARD_HAS_OLED;
    useWebLog = ESPARAM_ENABLE_WEBSERVER;
    useSerial = true;
    newData = true;
    logLineIdx = 0;
    logCharIdx = 0;
    webLogIdx = 0;
    
#if ESPARAM_BOARD_HAS_OLED
    Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
    }
    delay(1000);
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0, 7);
#endif
    
    update();
    // screenSaverTimeStamp = millis();
}

// void Printer::setHeader(const char *_header){
//     strcpy(headerText,_header);
//     newData = true;
// }


// we overload flush with the purpose of updating OLED and weblog, on demand
void Printer::flush(){
    update();
}

void Printer::update(){
#if ESPARAM_BOARD_HAS_OLED
    if(useOled && newData){
        display.clearDisplay();
        display.setTextSize(1);
        display.setCursor(0, 0);
        display.write(headerAText, PRINTER_HEADER_LENGTH);
        display.setCursor(0, 8);
        display.write(headerBText, PRINTER_HEADER_LENGTH);

        for(int i = 0; i < LOG_LINE_ROW_COUNT; i++){
            display.setCursor(0, i*8+8);
            display.write(logBuffer[(i+logLineIdx)%LOG_LINE_ROW_COUNT]);
        }
        display.display(); 
        newData = false;
    }
#endif
#if ESPARAM_ENABLE_WEBSERVER
    if(webLogAvailable() > 0){
        sendToWeblog(webLog);
        printer.clearWebLog();
    }
#endif
}

size_t Printer::write(uint8_t _c){
    if(useOled){
        bool newLine = false;
        if(_c == '\n'){
            newLine = true;
        }
        else if(_c == '\r'){
            // skip these
        }
        else {
            logBuffer[logLineIdx][logCharIdx] = _c;
            logCharIdx++;
            if(logCharIdx >= MAX_LOG_LINE_WIDTH){
                newLine = true;
            }
        }
        if(newLine){
            logCharIdx = 0;
            logLineIdx++;
            logLineIdx%= LOG_LINE_ROW_COUNT;
            memset(logBuffer[logLineIdx], 0, MAX_LOG_LINE_WIDTH);
        }
        newData = true;
    }
    if(useSerial) {
        Serial.write(_c);
    }
    if(useWebLog){
        if(webLogIdx < WEB_LOG_MAX_LENGTH){
            webLog[webLogIdx++] = _c;
        }
    }

    return 1;
}

void Printer::wakeDisplay(){
    // screenSaverTimeStamp = millis();
}

void Printer::clearWebLog(){
    memset(webLog, 0, WEB_LOG_MAX_LENGTH);
    webLogIdx = 0;
}

size_t Printer::webLogAvailable(){
    return webLogIdx;
}