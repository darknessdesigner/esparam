#include "ESParam.h"
#include "ESParam_conf.h"

MessageNotRecognizedCallbackFunction messageNotRecognizedCallbackFunction;

void setMessageNotRecognizedCallbackFunction(MessageNotRecognizedCallbackFunction callback){
    messageNotRecognizedCallbackFunction = callback;
    }

namespace {
    // multipurpose button
    // change to param tracker
    ParamLinker buttonZeroLinker;
    Bounce bounceZero = Bounce();
    // where to write information
    Print * printer;
    IPAddress wifiIp, wifiGateway, wifiSubnet;
    IPAddress oscOutIp;
#if ESPARAM_BOARD_HAS_ETHERNET
    IPAddress ethIp, ethGateway, ethSubnet;
#endif

    String ipString;
    DNSServer dnsServer;
    AsyncWebServer server(ESPARAM_WS_PORT);
    AsyncWebSocket ws("/webparam");
    unsigned int websocketClientId;
    bool websocketPushFlag = false;
    #define WEBSOCKET_LOGGER_SIZE 256
    char websocketMessageBuffer[WEBSOCKET_LOGGER_SIZE];
    unsigned long wifiPreviousMillis = 0;
    unsigned long wifiInterval = 10000;
    bool wifiSoftAp = false;
    JsonDocument epJsonDoc;
}

// all configuration parameters in one struct
ESParamConf epConf;

String processor(const String& var){
    Serial.println(var);
    if(var == "DEVICE_IP"){
        return String(
            wifiSoftAp ? WiFi.softAPIP().toString() : WiFi.localIP().toString()
        );
    }
    return String();
}
 
String noProcessor(const String& var){
    return String();
}
 

void printerUpdate(){
    printer->flush();
}

class CaptiveRequestHandler : public AsyncWebHandler {
public:
  CaptiveRequestHandler() {}
  virtual ~CaptiveRequestHandler() {}

  bool canHandle(AsyncWebServerRequest *request){
    //request->addInterestingHeader("ANY");
    return true;
  }

  void handleRequest(AsyncWebServerRequest *request) {
    request->send(LittleFS, "/index.html", String(), false, processor);
  }
};

//////////////////////////////////////////////////////////////////////////
// local methods
void reboot(){
    ESP.restart();
}
void websocketEventHandler(
        AsyncWebSocket * server, 
        AsyncWebSocketClient * client, 
        AwsEventType type, 
        void * arg, 
        uint8_t *data, 
        size_t len
    ){
    switch(type){
        case WS_EVT_CONNECT:
            websocketClientId = client->id();
            websocketPushFlag = true;
            break;
        case WS_EVT_DATA:
            {
                DeserializationError err = deserializeJson(epJsonDoc, data);
                if(err){
                    printer->printf("[ws] json fail :%s\n", err.c_str());
                }
                else {
                    // receive the message
                    JsonObject _obj = epJsonDoc.as<JsonObject>();
                    receiveJson(_obj, &paramCollector);
                }
            }
            break;
        case WS_EVT_DISCONNECT:
            printer->println("[ws] disconnected");
            break;
        case WS_EVT_PONG:
            break;
        case WS_EVT_ERROR:
            Serial.printf("ws[%s][%lu] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
            break;
    }
}

void WiFiEvent(WiFiEvent_t event){
    switch (event) {
#if ESPARAM_BOARD_HAS_ETHERNET
        case ARDUINO_EVENT_ETH_START:
            printer->println("[eth] Started");
            //set eth hostname here
            ETH.setHostname(epConf.deviceNameParam.v);
            break;
        case ARDUINO_EVENT_ETH_CONNECTED:
            printer->println("[eth] Connected");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP:
            printer->printf(
                "[eth] IP:%s FDUP:%s SPD:%iMbps\n",
                ETH.localIP().toString(),
                ETH.fullDuplex() ? "yes":"no",
                ETH.linkSpeed()
            );
            break;
        case ARDUINO_EVENT_ETH_DISCONNECTED:
            printer->println("[eth] Disconnected");
            // eth_connected = false;
            break;
        case ARDUINO_EVENT_ETH_STOP:
            printer->println("[eth] Stopped");
            // eth_connected = false;
            break;
#endif

        // // wifi
        case ARDUINO_EVENT_WIFI_STA_START:
            printer->println("[wifi] Started");
            //set eth hostname here
            break;
        case ARDUINO_EVENT_WIFI_STA_CONNECTED:
            printer->println("[wifi] Connected");
            break;
        case ARDUINO_EVENT_WIFI_STA_GOT_IP:
            printer->println("[wifi] IP:");
            printer->println(WiFi.localIP());
            break;
        case ARDUINO_EVENT_WIFI_STA_DISCONNECTED:
            if(wifiSoftAp == false){
                printer->println("[wifi] Disconnected");
            }
            break;
        case ARDUINO_EVENT_WIFI_STA_STOP:
            printer->println("[wifi] Stopped");
            break;
        default:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////////
/// spiffs save and load params
///

#if ESPARAM_ENABLE_FS

const char * configFileName = "/config.jso";
// save
void saveParamsLittleFS(){
    printer->printf("[params] saving %s\n", configFileName);
    if(LittleFS.exists(configFileName)){
        LittleFS.remove(configFileName);
    }
    File _file = LittleFS.open(configFileName, FILE_WRITE);
    if(_file){
        // ParamJson has a StaticJsonDocument for us to use
        epJsonDoc.clear();
        saveParams(epJsonDoc, &paramCollector);
        serializeJson(epJsonDoc, _file);
        _file.close();
        // serializeJsonPretty(epJsonDoc, printer);
        printer->println("\n[params] saved!");
    }
    else {
        printer->println("[params] failed save params");
    }
}

void loadParamsLittleFS(){
    File _file = LittleFS.open(configFileName);
    if(_file){
        DeserializationError err = deserializeJson(epJsonDoc, _file);
        if(err){
            printer->printf("[params] json fail :%s \n", err.c_str());
        }
        loadParams(epJsonDoc, &paramCollector);
        _file.close();
    }
    else {
        printer->println("[params] failed load params");
    }
}

void deleteConfig(){
    printer->printf("[params] deleting %s\n", configFileName);
    if(LittleFS.exists(configFileName)){
        LittleFS.remove(configFileName);
    }
}

#endif // ESPARAM_ENABLE_FS

#if ESPARAM_ENABLE_OSC
void myOscMessageParser( MicroOscMessage& receivedOscMessage) {
    if(!paramDispatchOSC(receivedOscMessage, &paramCollector)){
        if(messageNotRecognizedCallbackFunction){
            messageNotRecognizedCallbackFunction(receivedOscMessage);
            // testing
        }
    }
}

// public :
WiFiUDP udpOscSocket;
bool oscInputFlag;
MicroOscUdp<ESPARAM_MICROSC_BUFFER_SIZE> myMicroOscUdp(&udpOscSocket, oscOutIp, 9000);
#endif



//////////////////////////////////////////////////////////////////////////
// public
ParamCollector paramCollector;

const char * getDeviceName(){
    return epConf.deviceNameParam.v;
}
const char * getDeviceSSID(){
    return epConf.wifiSsidParam.v;
}
// exposed
// setup all the parameters used
// load config from flash
// start network.
// make sure to collect other paramters that are saved to flash before this.
void setupEsparam(Print * p){
    printer = p;
    printer->println("[ep] setting up");
    printerUpdate();
    bounceZero.attach(BUTTON_PIN, INPUT);
    bounceZero.interval(5);
    // device name will be used for humans to tell devices appart
    // could be used as hostname
    // should be settable from elsewhere in the code.
    // use  ESP.getEfuseMac()? to make unique default id?
    
    epConf.deviceNameParam.set("/config/device/name",  "naaaaaaaaaaaaaaaaame");
    sprintf(epConf.deviceNameParam.v, "esparam-%lli", ESP.getEfuseMac());
    epConf.deviceNameParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.deviceNameParam);
    // wifi chonk
    epConf.forceAccessPointParam.set("/config/wifi/ap_mode", 0);
    epConf.forceAccessPointParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.forceAccessPointParam);
    epConf.wifiEnableParam.set( "/config/wifi/enable",  ESPARAM_DEFAULT_WIFI_ENABLE);
    epConf.wifiEnableParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiEnableParam);
    epConf.wifiDhcpParam.set("/config/wifi/dhcp",    ESPARAM_DEFAULT_WIFI_DHCP);
    epConf.wifiDhcpParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiDhcpParam);
    epConf.wifiStaticIpParam.set("/config/wifi/ip",      ESPARAM_DEFAULT_STATIC_WIFI_IP);
    epConf.wifiStaticIpParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiStaticIpParam);
    
    epConf.wifiGatewayParam.set("/config/wifi/gateway", ESPARAM_DEFAULT_WIFI_GATEWAY);
    epConf.wifiGatewayParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiGatewayParam);
    
    epConf.wifiSubnetParam.set( "/config/wifi/subnet",  ESPARAM_DEFAULT_WIFI_SUBNET);
    epConf.wifiSubnetParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiSubnetParam);

    epConf.wifiSsidParam.set(   "/config/wifi/ssid",    ESPARAM_DEFAULT_WIFI_SSID);
    epConf.wifiSsidParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.wifiSsidParam);
    
    epConf.wifiPswdParam.set(   "/config/wifi/pswd",    ESPARAM_DEFAULT_WIFI_PSWD);
    epConf.wifiPswdParam.saveType = SAVE_ON_REQUEST;
    epConf.wifiPswdParam.readAccess = false;
    paramCollector.add(&epConf.wifiPswdParam);

#if ESPARAM_BOARD_HAS_ETHERNET
    // ethernet
    epConf.ethEnableParam.set( "/config/eth/enable",  ESPARAM_DEFAULT_ETH_ENABLE);
    epConf.ethEnableParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.ethEnableParam);
    epConf.ethIpParam.set(     "/config/eth/ip",      ESPARAM_DEFAULT_STATIC_ETH_IP);
    epConf.ethIpParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.ethIpParam);
    epConf.ethGatewayParam.set("/config/eth/gateway", ESPARAM_DEFAULT_ETH_GATEWAY);
    epConf.ethGatewayParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.ethGatewayParam);
    epConf.ethSubnetParam.set( "/config/eth/subnet",  ESPARAM_DEFAULT_ETH_SUBNET);
    epConf.ethSubnetParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.ethSubnetParam);    
    // ethDhcpParam.set(   "/config/eth/DHCP",    ESPARAM_DEFAULT_ETH_DHCP);
    // epConf.ethDhcpParam.saveType = SAVE_ON_REQUEST;
    // paramCollector.add(&epConf.ethDhcpParam);
#endif


    // other params 
#if ESPARAM_ENABLE_OSC
    epConf.oscInPortParam.set("/config/osc/in/port", 0, 50000, ESPARAM_DEFAULT_OSC_IN_PORT);
    epConf.oscInPortParam.saveType = SAVE_ON_REQUEST;
    epConf.oscInPortParam.inputType = NUMBER;
    paramCollector.add(&epConf.oscInPortParam);

    epConf.oscOutPortParam.set("/config/osc/out/port", 0, 50000, ESPARAM_DEFAULT_OSC_OUT_PORT);
    epConf.oscOutPortParam.saveType = SAVE_ON_REQUEST;
    epConf.oscOutPortParam.inputType = NUMBER;
    paramCollector.add(&epConf.oscOutPortParam);
    

    epConf.oscOutIpParam.set("/config/osc/out/ip", ESPARAM_DEFAULT_OSC_OUT_IP);
    epConf.oscOutIpParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&epConf.oscOutIpParam);
#endif    
    
    buttonZeroLinker.paramAddress.set("/config/button/param","none");
    buttonZeroLinker.paramAddress.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&buttonZeroLinker.paramAddress);

    epConf.rebootParam.set("/config/reboot");
    epConf.rebootParam.setCallback(reboot);
    paramCollector.add(&epConf.rebootParam);

    // init the spiffs file system
    // load the saves parameters from flash chip
#if ESPARAM_ENABLE_FS
    epConf.saveParam.set("/config/save");
    epConf.saveParam.setCallback(saveParamsLittleFS);
    paramCollector.add(&epConf.saveParam);
    
    epConf.deleteConfigParam.set("/config/delete");
    epConf.deleteConfigParam.setCallback(deleteConfig);
    paramCollector.add(&epConf.deleteConfigParam);
    // init the file system
    if(!LittleFS.begin()){
        printer->println("[lfs] An Error has occurred while mounting LittleFS");
        printer->println("[lfs] may need to format please wait...");
        printerUpdate();
        LittleFS.begin(FORMAT_FS_IF_FAILED);
    }
    else {
        printer->println("[lfs] done init");
    }
    // printer->println("[lfs] formating");
    // printerUpdate();
    // LittleFS.format();
    // load the saves parameters from flash chip
    loadParamsLittleFS();
#endif

    printer->println("[ep] done setup");
    printerUpdate();

}


// exposed
void startNetwork(){
    printer->println("[ep] starting network");
    printerUpdate();
    // IPAddress device_dns(0,0,0,0);
    WiFi.onEvent(WiFiEvent);
#if ESPARAM_BOARD_HAS_ETHERNET
    if(epConf.ethEnableParam.v){
        ethIp.fromString(epConf.ethIpParam.v);
        ethGateway.fromString(epConf.ethGatewayParam.v);
        ethSubnet.fromString(epConf.ethSubnetParam.v);
        ETH.begin();
        ETH.config(
            ethIp, 
            ethGateway,
            ethSubnet//,
            // device_dns,
            // device_dns
        );
    }
#endif
    // wifi setup
    if(epConf.wifiEnableParam.v){
        wifiIp.fromString(epConf.wifiStaticIpParam.v);
        wifiGateway.fromString(epConf.wifiGatewayParam.v);
        wifiSubnet.fromString(epConf.wifiSubnetParam.v);

        wifiSoftAp = epConf.forceAccessPointParam.v;
        printer->println("[wifi] PRESS BUTTON FOR AP MODE");
        printerUpdate();
        unsigned long stamp = millis();
        WiFi.setHostname(epConf.deviceNameParam.v);
        while(millis() < stamp+ESPARAM_TRIGGER_AP_MODE_TIMEOUT){
            bounceZero.update();
            if(bounceZero.changed()){
                if(bounceZero.read() == 0 || wifiSoftAp){
                    printer->println("[wifi] AP MODE!");
                    wifiSoftAp = true;
                    break;
                }
            }
        }
        if(wifiSoftAp == false){
            printer->printf("[wifi] enabled, connecting to %s\n", epConf.wifiSsidParam.v);
            printer->printf("[wifi] pswd to %s\n", epConf.wifiPswdParam.v);
            printerUpdate();
            WiFi.mode(WIFI_STA);
            if(epConf.wifiDhcpParam.v){
                WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
            }
            else {
                WiFi.config(
                    wifiIp,
                    wifiGateway,
                    wifiSubnet,
                    IPAddress(8,8,8,8)
                );
            }
            WiFi.begin(epConf.wifiSsidParam.v, epConf.wifiPswdParam.v); // third argument is channel should be a param
            long t = millis();
            while (WiFi.status() != WL_CONNECTED) {
                printer->print('.');
                delay(100);
                if(t+ESPARAM_WIFI_CONNECT_TIMEOUT < millis()) {
                    wifiSoftAp = true;
                    break;
                }
            }
        }
        
        if(wifiSoftAp == true){
            WiFi.mode(WIFI_MODE_AP);
            // maybe this should be fixed? as well as a unique AP ssid?
            // that way fallback options are less likely to be some random shit?
            WiFi.softAP(epConf.deviceNameParam.v, ESPARAM_WIFI_AP_PSWD);
            printer->print("[wifi] ap up:");
            printer->println(epConf.deviceNameParam.v);
            printer->print("[wifi] ip: ");
            printer->println(WiFi.softAPIP());
            ipString = WiFi.softAPIP().toString();
        }
        else {
            printer->println("[wifi] connected");
            printer->print("[wifi] ip: ");
            printer->println(WiFi.localIP());
            ipString = WiFi.localIP().toString();
        }
        printerUpdate();

    }
    // other network related stuff
    if(epConf.wifiEnableParam.v){
        // MDNS.begin also gets called in ArduinoOTA.begin or something
        if (!MDNS.begin(epConf.deviceNameParam.v)) {
            printer->println("Error setting up MDNS responder!");
        }
#if ESPARAM_ENABLE_OSC
        udpOscSocket.begin(epConf.oscInPortParam.v);
        oscOutIp.fromString(epConf.oscOutIpParam.v);
        myMicroOscUdp.setDestination(oscOutIp, epConf.oscOutPortParam.v);
        // oscInputFlag = false;
        MDNS.addService("_osc", "_udp", epConf.oscInPortParam.v);
        printer->printf("[osc] listening on %i\n", epConf.oscInPortParam.v);
        printer->printf("[osc] sending to %s:%i\n", epConf.oscOutIpParam.v, epConf.oscOutPortParam.v);
        printerUpdate();
#endif    
        // else {
            // MDNS.addService("_param", "_tcp", ESPARAM_WS_PORT);
        // }
#if ESPARAM_ENABLE_OTA
        ArduinoOTA.setHostname(epConf.deviceNameParam.v);
        // ArduinoOTA.setMdnsEnabled(false);
        ArduinoOTA.onStart([]() {
            // maybe should add an optionnal callback here to let the rest of the code know
            String type;
            printer->print("[ota] Start updating ");
            printer->println(ArduinoOTA.getCommand() == U_FLASH ? "sketch" : "filesystem");
            // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        });
        ArduinoOTA.onEnd([]() {
            printer->println("\n[ota]End");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            printer->printf("[ota] Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            printer->printf("[ota] Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) printer->println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) printer->println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) printer->println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) printer->println("Receive Failed");
            else if (error == OTA_END_ERROR) printer->println("End Failed");
            printerUpdate();
        });
        ArduinoOTA.begin();
#endif // ESPARAM_ENABLE_OTA

// maybe we should wrap this also?
        ws.onEvent(websocketEventHandler);
        server.addHandler(&ws);

#if ESPARAM_ENABLE_WEBSERVER
        dnsServer.start(53,"*",WiFi.softAPIP());
        server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
            request->send(LittleFS, "/index.html", String(), false, processor);
        });
        server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
            request->send(LittleFS, "/style.css", String(), false, noProcessor);
        });
        server.on("/webparam.js", HTTP_GET, [](AsyncWebServerRequest *request){
            request->send(LittleFS, "/webparam.js", String(), false, noProcessor);
        });
        server.on("/iro.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
            request->send(LittleFS, "/iro.min.js", String(), false, noProcessor);
        });
        server.addHandler(new CaptiveRequestHandler()).setFilter(ON_AP_FILTER);
#endif // ESPARAM_ENABLE_WEBSERVER
        server.begin();
    }
    else {
        printer->println("[ep] wifi is off");
    }
    printer->println("[ep] done starting network");
    printerUpdate();
}

// exposed
void updateEsparam(){
    dnsServer.processNextRequest();
    unsigned long currentMillis = millis();
    // if WiFi is down, try reconnecting
    if(wifiSoftAp == false){
        if ((WiFi.status() != WL_CONNECTED) && (currentMillis - wifiPreviousMillis >= wifiInterval)) {
            printer->print("[ep]Reconnecting to WiFi...");
            printer->println(millis());
            WiFi.disconnect();
            WiFi.reconnect();
            wifiPreviousMillis = currentMillis;
        }
    }

    if(epConf.wifiEnableParam.v){
#if ESPARAM_ENABLE_OSC
        myMicroOscUdp.onOscMessageReceived( myOscMessageParser );
#endif
#if ESPARAM_ENABLE_OTA
        ArduinoOTA.handle();
#endif
    }

    if(websocketPushFlag){
        websocketPushFlag = false;
        printer->println("[ws] connected");
        for(const auto& _client: ws.getClients()){
            if(_client->id() == websocketClientId){
                for(int i = 0; i < paramCollector.index; i++) {
                    epJsonDoc.clear();
                    JsonObject jsonParam = epJsonDoc["add"].to<JsonObject>();
                    // compose a JsonObject with a parameter
                    jsonifyParam(jsonParam, paramCollector.pointers[i], true);
                    // send the json over websocket
                    size_t len = measureJson(epJsonDoc)+1;
                    // if(len < PARAM_WEBSOCKET_BUFFER_SIZE) {
                    // serializeJsonPretty(epJsonDoc, Serial);
                    serializeJson(epJsonDoc, websocketMessageBuffer, len);
                    while(_client->queueIsFull()) vTaskDelay(2); 
                    _client->text(websocketMessageBuffer);    
                }
                break;
            }
        }
    }

    if(buttonZeroLinker.needsUpdate()){
        if(buttonZeroLinker.findParam(&paramCollector)){
            printer->printf("[input] b0 linked:%s",buttonZeroLinker.targetParam->address);
        }
    }
    if(buttonZeroLinker.isLinked()){
        bounceZero.update();
        if(bounceZero.changed()){
            if(bounceZero.read() == 0){
                buttonZeroLinker.receiveBang();
            }
        }
    }

    printerUpdate();
}

void sendToWeblog(char *_log){
    if(ws.count() > 0){
        epJsonDoc.clear();
        JsonObject jsonLog = epJsonDoc["log"].to<JsonObject>();
        jsonLog["text"] = _log;
        // send the json over websocket
        size_t len = measureJson(epJsonDoc)+1;
        // if(len < PARAM_WEBSOCKET_BUFFER_SIZE) {
        if(true) {
            if(ws.availableForWriteAll()){
                serializeJson(epJsonDoc, websocketMessageBuffer, len);
                ws.textAll(websocketMessageBuffer);
                // logger.clearWebLog();
            }
            else {
                printer->println("[ws] not avail");
            }
        }
        else {
            printer->println("[ws] message too big for buffer");
            // logger.clearWebLog();
        }
    }
}

void websocketSendMessage(char * _msg){
    if(ws.availableForWriteAll()){
        ws.textAll(_msg);
    }
    else {
        printer->println("[ws] not avail");
    }
}

String getDeviceIp(){
    return ipString;
    // if(wifiSoftAp){
    //     return WiFi.softAPIP().toString();
    // }
    // else {
    //     WiFi.localIP().toString();
    // }
}

bool esparamIsAP(){
    return wifiSoftAp;
}